/*
 * Gulp custom variables file.
 *
 * Set your local variables in this file.
 * Valid variables for 'domain' are: 'auto', null, or a domain name.
 * null will disable BrowserSync.
 *
 * Valid variables for 'environment' are: 'lando' and 'devdesktop'.
 */

module.exports = {
  'domain': 'LANDO URL',
  'environment': 'lando',
  'sassLintConfigFile': 'sass-lint.yml'
};
